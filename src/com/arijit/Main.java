package com.arijit;

import com.arijit.Shopping.Customer;
import com.arijit.Shopping.Inventory;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Inventory inventory = Inventory.getInstance();
        Scanner scanner = new Scanner(System.in);
        System.out.println("!!Welcome to ABC Shopping app!!");
        int input1;
        int input2;
        Customer customer=null;
        String choice;
        do{
            System.out.println("""
                    Who is using ?
                    1. Admin
                    2. User""");
            System.out.print("Enter your choice:\t");
            input1 = Integer.parseInt(scanner.nextLine());
            switch (input1){
                case (1) ->{
                    System.out.println("Welcome Admin!");
                    do{
                        System.out.println("""
                                Main Menu:
                                1. Add Product
                                2. Remove Product""");
                        System.out.print("Enter your choice:\t");
                        input2 = Integer.parseInt(scanner.nextLine());
                        switch (input2) {
                            case (1) -> {
                                do {
                                    inventory.addProduct();
                                    System.out.println("To add more product press 'y' or press 'n'");
                                } while (!scanner.nextLine().equals("n"));
                            }
                            case (2) -> {
                                do {
                                    inventory.removeProduct();
                                    System.out.println("To remove more product press 'y' or press 'n'");
                                } while (!scanner.nextLine().equals("n"));
                            }
                        }
                        System.out.println("To continue as Admin press 'y', to end press 'n' ");
                    }while (!scanner.nextLine().equals("n"));
                }
                case (2) ->{
                    System.out.println("Welcome User!");
                    System.out.println("""
                            Main Menu:
                            1. Create Customer
                            2. Customer Login""");
                    System.out.print("Enter your choice:\t");
                    input2 = Integer.parseInt(scanner.nextLine());
                    switch (input2){
                        case (1) -> customer=Customer.createCustomer();
                        case (2) -> customer=Customer.customerLogin();
                        default -> System.out.println("Enter some valid choice");

                    }
                    if(customer==null)
                        break;
                    do {
                        System.out.println("""
                                Customer options:
                                1. Add product to cart
                                2. Remove product from cart
                                3. Show cart detail
                                4. return""");
                        System.out.print("Enter your choice:\t");
                        input2 = Integer.parseInt(scanner.nextLine());
                        switch (input2) {
                            case (1) -> {
                                do {
                                    inventory.showDetail();
                                    System.out.println("Enter the product number to purchase with quantity separated with ';' (id;quantity): ");
                                    String[] buy = scanner.nextLine().split(";");
                                    customer.addToCart(inventory.productList.get(Integer.parseInt(buy[0])), Integer.parseInt(buy[1]));
                                    System.out.println("To add more product press 'y' or press 'n'");
                                } while (!scanner.nextLine().equals("n"));
                            }
                            case (2) -> {
                                do {
                                    customer.inCartItemList();
                                    System.out.println("Enter the product number to remove with quantity separated with ';' (id;quantity): ");
                                    String[] remove = scanner.nextLine().split(";");
                                    customer.removeFromCart(inventory.productList.get(Integer.parseInt(remove[0])), Integer.parseInt(remove[1]));
                                    System.out.println("To remove more product press 'y' or press 'n'");
                                } while (!scanner.nextLine().equals("n"));
                            }
                            case (3) -> customer.showCartDetails();
                            default -> System.out.println("Enter some valid choice");
                        }
                        System.out.println("To continue as Customer press 'y', to end press 'n' ");
                    }while (!scanner.nextLine().equals("n"));
                }
            }
            System.out.println("To continue with app press 'y', to end press 'n' ");
            choice = scanner.nextLine();
        }while (choice.equals("y"));
    }
}
