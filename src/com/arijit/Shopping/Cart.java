package com.arijit.Shopping;

import java.util.HashMap;
import java.util.Map;

public class Cart {
    static int count =0;
//    static ArrayList<Cart> CartList = new ArrayList<>();
    public HashMap<Product,Integer> shoppingList = new HashMap<>();
    public final int Id;
    Customer Owner ;
    Inventory inventory = Inventory.getInstance();

    private Cart(Customer owner)
    {
        this.Owner=owner;
        count++;
        this.Id=count;
    }

    static Cart getCart(Customer owner)
    {
        return new Cart(owner);
    }

    public void addProduct(Product product, int quantity)
    {
        if(inventory.productQuantity.get(product.id)<quantity)
        {
            quantity=inventory.productQuantity.get(product.id);
        }

        if(!shoppingList.containsKey(product))
        {
            shoppingList.put(product, quantity);
        }
        else {

            shoppingList.replace(product, shoppingList.get(product)+quantity);
        }
        inventory.productQuantity.replace(product.id, inventory.productQuantity.get(product.id)-quantity);
    }

    public void removeProduct(Product product, int quantity)
    {
        if(!shoppingList.containsKey(product))
        {
            System.out.println("This product doesn't exist in the cart");
        }
        else {
            if(shoppingList.get(product)<quantity)
                quantity= shoppingList.get(product);
            shoppingList.replace(product, shoppingList.get(product)-quantity);
            inventory.productQuantity.replace(product.id, inventory.productQuantity.get(product.id)+quantity);
        }
    }

    public void ItemList()
    {
        for(Map.Entry<Product,Integer> entry:shoppingList.entrySet())
        {
            System.out.println(entry.getKey().id+". "+entry.getKey().getName()+" - "+entry.getValue());
        }
    }

    public void showDetails()
    {
        System.out.println("Cart Details: ");
        for(Map.Entry<Product,Integer> entry: shoppingList.entrySet())
        {
            System.out.println("Product Name: "+entry.getKey().getName()+ " Product Quantity: "+entry.getValue()+" Product Net Price: "+entry.getKey().getPrice()*entry.getValue());
        }
        System.out.println("Total Price is " +this.CartTotal());
    }

    public int CartTotal()
    {
        int price =0;
        for(Map.Entry<Product,Integer> entry: shoppingList.entrySet())
        {
            price+=entry.getKey().getPrice()*entry.getValue();
        }
        return price;
    }
}
