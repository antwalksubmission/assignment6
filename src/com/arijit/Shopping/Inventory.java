package com.arijit.Shopping;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Inventory {

    public HashMap<Integer,Integer> productQuantity = new HashMap<>();
    public HashMap<Integer, Product> productList = new HashMap<>();

    private static Inventory singleInstance = null;

    Scanner scanner = new Scanner(System.in);

    private Inventory() {

    }

    public static Inventory getInstance()
    {
        if(singleInstance==null)
        {
            singleInstance = new Inventory();
        }
        return singleInstance;
    }

    public void addProduct()
    {
        System.out.println("Enter new Product Details separated by ';' (Name, Brand, Price): ");
        String[] input = scanner.nextLine().split(";");
        Product product = new Product(input[0],input[1],Integer.parseInt(input[2]));
        System.out.println("Enter the quantity to add: ");
        int qty = Integer.parseInt(scanner.nextLine());
        productQuantity.put(product.id, qty);
        productList.put(product.id, product);
        System.out.println("Product added successfully! Product detail: ");
        System.out.println(product);
    }

    public void removeProduct()
    {
        System.out.println("Enter the product id to be deleted");
        int product_id = Integer.parseInt(scanner.nextLine());
        if(productQuantity.containsKey(product_id))
            productQuantity.remove(product_id);
        else {
            System.out.println("Error!! This Product_id is not valid");
        }
    }

    public void showDetail()
    {
        System.out.println("Here are available Products with their Quantities:");
        for(Map.Entry<Integer,Integer> entry: productQuantity.entrySet())
        {
            System.out.println(entry.getKey()+". "+productList.get(entry.getKey())+":\t"+entry.getValue());
        }
    }
}
