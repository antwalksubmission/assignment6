package com.arijit.Shopping;

import java.util.HashMap;
import java.util.Scanner;

public class Customer {

    static Scanner scanner = new Scanner(System.in);
    public static HashMap<Integer, Customer> customerList = new HashMap<>();
    static String[] input;
    static int count=0;

    private String Name;
    private String Phone;
    private String Email;
    public final int Id;

    private final Cart myCart;

    @Override
    public String toString() {
        return "Customer{" +
                "Name='" + Name + '\'' +
                ", Phone='" + Phone + '\'' +
                ", Email='" + Email + '\'' +
                ", Id=" + Id +
                '}';
    }

    public Customer(String name, String phone, String email) {
        this.Name = name;
        this.Phone = phone;
        this.Email = email;
        count++;
        this.Id =count;
        this.myCart=Cart.getCart(this);
    }

    public void addToCart(Product product, int quantity)
    {
        this.myCart.addProduct(product,quantity);
    }

    public void removeFromCart(Product product, int quantity)
    {
        this.myCart.removeProduct(product,quantity);
    }

    public void showCartDetails()
    {
        myCart.showDetails();
    }

    public void inCartItemList()
    {
        myCart.ItemList();
    }

    public static Customer createCustomer()
    {
        System.out.println("Enter your details: Name, Phone number and email separated by ';' (Name;Phone:Email)");
        input = scanner.nextLine().split(";");
        Customer customer = new Customer(input[0],input[1],input[2]);
        customerList.put(customer.Id, customer);
        System.out.println("Customer created successfully. Details are: ");
        System.out.println(customer);
        return customer;
    }

    public static Customer customerLogin()
    {
        int customer_id;
        do
        {
            System.out.println("Enter your Customer Id");
            customer_id=Integer.parseInt(scanner.nextLine());
            if(!customerList.containsKey(customer_id))
            System.out.println("Wrong Id");
        }while (!customerList.containsKey(customer_id));
        return customerList.get(customer_id);
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        this.Name = name;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        this.Phone = phone;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        this.Email = email;
    }

    public Cart getMyCart() {
        return myCart;
    }
}
