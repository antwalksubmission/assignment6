package com.arijit.Shopping;

public class Product {

    static int count = 0;

    public final int id;

    private String Name;
    private String Brand;
    private int Price;

    public Product(String name, String brand, int price) {
        Name = name;
        Brand = brand;
        this.Price = price;
        count++;
        this.id = count;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", Name='" + Name + '\'' +
                ", Brand='" + Brand + '\'' +
                ", Price=" + Price +
                '}';
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getBrand() {
        return Brand;
    }

    public void setBrand(String brand) {
        Brand = brand;
    }

    public int getPrice() {
        return Price;
    }

    public void setPrice(int price) {
        this.Price = price;
    }
}
